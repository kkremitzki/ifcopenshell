Source: ifcopenshell
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Kurt Kremitzki <kurt@kwk.systems>
Section: science
Priority: optional
Build-Depends: cmake,
               debhelper (>= 9~),
               dh-exec,
               dh-python,
               libboost-atomic-dev,
               libboost-chrono-dev,
               libboost-date-time-dev,
               libboost-program-options-dev,
               libboost-regex-dev,
               libboost-system-dev,
               libboost-thread-dev,
               libicu-dev,
               libocct-data-exchange-dev,
               libocct-foundation-dev,
               libocct-modeling-data-dev,
               libocct-ocaf-dev,
               libocct-visualization-dev,
               python3-dev,
               swig
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/science-team/ifcopenshell
Vcs-Git: https://salsa.debian.org/science-team/ifcopenshell.git
Homepage: https://github.com/IfcOpenShell/IfcOpenShell
Short-Desc: Open source IFC library and geometry engine
Long-Desc: IfcOpenShell is an open source (LGPL) software library that helps users and
 software developers to work with the IFC file format. The IFC file format can
 be used to describe building and construction data. The format is commonly used
 for Building Information Modelling.
 .
 IfcOpenShell uses Open CASCADE internally to convert the implicit geometry in
 IFC files into explicit geometry that any software CAD or modelling package can
 understand.

Package: ifcopenshell0
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: ${S:Short-Desc} - binaries & examples
 ${S:Long-Desc}
 .
 This package contains the binaries and examples for IfcOpenShell.

Package: libifcopenshell0
Section: libs
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: ${S:Short-Desc} - shared libraries
 ${S:Long-Desc}
 .
 This package contains the shared libraries for IfcOpenShell.

Package: libifcopenshell-dev
Section: libdevel
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libifcopenshell0 (<< ${binary:Version}+1~),
         libifcopenshell0 (>= ${binary:Version})
Description: ${S:Short-Desc} - development files
 ${S:Long-Desc}
 .
 This package contains the development files for IfcOpenShell.

Package: python3-ifcopenshell
Section: python
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends}
Description: ${S:Short-Desc} - Python 3 bindings
 ${S:Long-Desc}
 .
 This package contains the Python 3 bindings for IfcOpenShell.
